package net.tncy.twitchgame;


import net.tncy.validator.constraints.books.ISBN;

public class Book {
    private String title;
    @ISBN
    private String isbn;
    private String author;

    public Book() {
        this.author = "None";
        this.title = "None";
        this.isbn = "AAZEAFVKEAR";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
