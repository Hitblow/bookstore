package net.tncy.twitchgame;

import net.tncy.twitchgame.Book;

import java.util.ArrayList;

public class BookService {
    private ArrayList<Book> list = new ArrayList<>();
    public void addBook(Book b) {
        list.add(b);
    }

    public ArrayList<Book> getList(){
        return list;
    }

    public void removeBook (Book b) {
        list.remove(b);
    }


}